import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SousTraitantListComponent} from './sous-traitant-list/sous-traitant-list.component';
import {CreateSousTraitantComponent} from './create-sous-traitant/create-sous-traitant.component';

const routes: Routes = [
  {
    path: 'list',
    component: SousTraitantListComponent
  },
  {
    path: 'create',
    component: CreateSousTraitantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SousTraitantRoutingModule {
}
