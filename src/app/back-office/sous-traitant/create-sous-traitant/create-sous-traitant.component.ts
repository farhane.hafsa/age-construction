import { Component, OnInit } from '@angular/core';
export interface Chantier {
  nom: string;
  ville: string;
}

const ELEMENT_DATA: Chantier[] = [
  { nom: 'Travaux de qualitatifs bordures granit ', ville: 'Marseille'},
  { nom: 'Déconstruction de 3 pavillons ', ville: 'Avignon'},
  { nom: 'Pose de mur de soutènement ', ville: 'Casis'},
  { nom: 'Déconstruction des vestiaires, désamiantage ', ville: 'Marseille'},
];
export interface Domaine {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-create-sous-traitant',
  templateUrl: './create-sous-traitant.component.html',
  styleUrls: ['./create-sous-traitant.component.scss']
})
export class CreateSousTraitantComponent implements OnInit {

  domaines: Domaine[] = [
    {value: 'domaine-1', viewValue: 'Préparation de terrain'},
    {value: 'domaine-2', viewValue: 'Gros OEuvre'},
    {value: 'domaine-3', viewValue: 'Charpente'},
    {value: 'domaine-4', viewValue: 'Couverture'},
    {value: 'domaine-5', viewValue: 'Étanchéité'},
    {value: 'domaine-6', viewValue: 'Revêtement de façade'},
    {value: 'domaine-7', viewValue: 'Menuiserie'},
    {value: 'domaine-8', viewValue: 'Plâtrerie'}
  ];

  displayedColumns: string[] = ['nom', 'ville' , 'actions'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
