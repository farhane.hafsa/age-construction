import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SousTraitantRoutingModule } from './sous-traitant-routing.module';
import { SousTraitantListComponent } from './sous-traitant-list/sous-traitant-list.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {CreateSousTraitantComponent} from './create-sous-traitant/create-sous-traitant.component';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [SousTraitantListComponent, CreateSousTraitantComponent],
  imports: [
    CommonModule,
    SousTraitantRoutingModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule
  ]
})
export class SousTraitantModule { }
