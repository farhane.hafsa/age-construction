import { Component, OnInit } from '@angular/core';
export interface SousTraitants {
  nom: string;
  prenom: string;
  siret: string;
}

const ELEMENT_DATA: SousTraitants[] = [
  { nom: 'DOE', prenom: 'Jhon' , siret: '362 521 879 00034'},
  { nom: 'NIABA', prenom: 'Victoria', siret: '589 521 879 00034'},
  { nom: 'FARHANE', prenom: 'Hafsa', siret: '306 123 879 00034'},
  { nom: 'DUPONT', prenom: 'Jean', siret: '265 521 879 1985'},
  { nom: 'HOPPER', prenom: 'Ava', siret: '452 521 879 0258'},
  { nom: 'CLARKE', prenom: 'Grace', siret: '158 521 879 00034'},
];

@Component({
  selector: 'app-sous-traitant-list',
  templateUrl: './sous-traitant-list.component.html',
  styleUrls: ['./sous-traitant-list.component.scss']
})
export class SousTraitantListComponent implements OnInit {

  displayedColumns: string[] = ['nom', 'prenom', 'siret' , 'actions'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
