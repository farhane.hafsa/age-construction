import { Component, OnInit } from '@angular/core';
export interface User {
  nom: string;
  prenom: string;
  email: string;
  UtiRole: string;
}

const ELEMENT_DATA: User[] = [
  { nom: 'DOE', prenom: 'Jhon' , email: 'Doe.Jhon@gmail.com' , UtiRole: 'administrateur'},
  { nom: 'NIABA', prenom: 'Victoria', email: 'VictoriaN@gmail.com' , UtiRole: 'chef de chantier'},
  { nom: 'FARHANE', prenom: 'Hafsa', email: 'Farhane1254@gmail.com' , UtiRole: 'chef de chantier'},
  { nom: 'DUPONT', prenom: 'Emélie', email: 'DupontE@gmail.com' , UtiRole: 'Secrétaire'},
  { nom: 'HOPPER', prenom: 'Ava', email: 'hopper-ava@gmail.com' , UtiRole: 'Chef de chantier'},
  { nom: 'CLARKE', prenom: 'Grace', email: 'Clare.grace@gmail.com' , UtiRole: 'Ingénieure'},
];

@Component({
  selector: 'app-gestion-des-comptes',
  templateUrl: './gestion-des-comptes.component.html',
  styleUrls: ['./gestion-des-comptes.component.scss']
})
export class GestionDesComptesComponent implements OnInit {

  displayedColumns: string[] = ['nom', 'prenom', 'email' , 'UtiRole' , 'actions'];
  dataSource = ELEMENT_DATA;

  constructor() { }

  ngOnInit(): void {
  }

}
