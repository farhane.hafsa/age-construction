import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {BackOfficeTemplateComponent} from './back-office-template/back-office-template.component';
import {RechercheAvanceComponent} from './recherche-avance/recherche-avance.component';
import {GestionDesComptesComponent} from './gestion-des-comptes/gestion-des-comptes.component';


const routes: Routes = [
  {
    path: '',
    component: BackOfficeTemplateComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'sous-traitant',
        loadChildren: () => import('./sous-traitant/sous-traitant.module').then(m => m.SousTraitantModule),
      },
      {
        path: 'evaluation',
        loadChildren: () => import('./evaluation/evaluation.module').then(m => m.EvaluationModule),
      },
      {
        path: 'recherche-avance',
        component: RechercheAvanceComponent
      },
      {
        path: 'gestion-des-comptes',
        component: GestionDesComptesComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackOfficeRoutingModule {
}
