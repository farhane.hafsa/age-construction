import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EvaluationRoutingModule } from './evaluation-routing.module';
import { EvaluationListComponent } from './evaluation-list/evaluation-list.component';
import { CreateEvaluationComponent } from './create-evaluation/create-evaluation.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [EvaluationListComponent, CreateEvaluationComponent],
  imports: [
    CommonModule,
    EvaluationRoutingModule,
    MatFormFieldModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule,
    MatInputModule
  ]
})
export class EvaluationModule { }
