import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EvaluationListComponent} from './evaluation-list/evaluation-list.component';
import {CreateEvaluationComponent} from './create-evaluation/create-evaluation.component';

const routes: Routes = [
  {
    path: 'list',
    component: EvaluationListComponent
  },
  {
    path: 'create',
    component: CreateEvaluationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EvaluationRoutingModule { }
