import { Component, OnInit } from '@angular/core';
export interface Evaluation {
  nom: string;
  prenom: string;
}

const ELEMENT_DATA: Evaluation[] = [
  { nom: 'DOE', prenom: 'Jhon' },
  { nom: 'NIABA', prenom: 'Victoria'},
  { nom: 'FARHANE', prenom: 'Hafsa'},
  { nom: 'DUPONT', prenom: 'Jean'},
  { nom: 'HOPPER', prenom: 'Ava'},
  { nom: 'CLARKE', prenom: 'Grace'},
];
@Component({
  selector: 'app-evaluation-list',
  templateUrl: './evaluation-list.component.html',
  styleUrls: ['./evaluation-list.component.scss']
})
export class EvaluationListComponent implements OnInit {
  displayedColumns: string[] = ['nom', 'prenom', 'evaluations' , 'actions'];
  dataSource = ELEMENT_DATA;
  constructor() { }

  ngOnInit(): void {
  }

}
