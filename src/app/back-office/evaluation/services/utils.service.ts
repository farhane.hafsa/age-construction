import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private http: HttpClient
  ) {
  }

  fetchCities(): Observable<any> {
    return this.http.get('assets/data/fr.json').pipe(
      map((cities: Array<any>) => {
        return cities.map(city => city.city);
      })
    );
  }
}
