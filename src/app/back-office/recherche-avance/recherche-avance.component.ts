import { Component, OnInit } from '@angular/core';
export interface SousTraitants {
  nom: string;
  prenom: string;
  siret: string;
}

const ELEMENT_DATA: SousTraitants[] = [
  { nom: 'DOE', prenom: 'Jhon' , siret: '362 521 879 00034'},
  { nom: 'NIABA', prenom: 'Victoria', siret: '589 521 879 00034'},
  { nom: 'FARHANE', prenom: 'Hafsa', siret: '306 123 879 00034'},
  { nom: 'DUPONT', prenom: 'Jean', siret: '265 521 879 1985'},
  { nom: 'HOPPER', prenom: 'Ava', siret: '452 521 879 0258'},
  { nom: 'CLARKE', prenom: 'Grace', siret: '158 521 879 00034'},
];

export interface Evaluation {
  nom: string;
  prenom: string;
}

const ELEMENT_DATA2: Evaluation[] = [
  { nom: 'DOE', prenom: 'Jhon' },
  { nom: 'NIABA', prenom: 'Victoria'},
  { nom: 'FARHANE', prenom: 'Hafsa'},
  { nom: 'DUPONT', prenom: 'Jean'},
  { nom: 'HOPPER', prenom: 'Ava'},
  { nom: 'CLARKE', prenom: 'Grace'},
];
@Component({
  selector: 'app-recherche-avance',
  templateUrl: './recherche-avance.component.html',
  styleUrls: ['./recherche-avance.component.scss']
})
export class RechercheAvanceComponent implements OnInit {

  displayedColumns: string[] = ['nom', 'prenom', 'siret'];
  dataSource = ELEMENT_DATA;

  displayedColumns2: string[] = ['nom', 'prenom', 'evaluations' ];
  dataSource2 = ELEMENT_DATA2;

  constructor() { }

  ngOnInit(): void {
  }

}
