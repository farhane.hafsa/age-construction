import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BackOfficeRoutingModule} from './back-office-routing.module';
import {BackOfficeTemplateComponent} from './back-office-template/back-office-template.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {RechercheAvanceComponent} from './recherche-avance/recherche-avance.component';
import {GestionDesComptesComponent} from './gestion-des-comptes/gestion-des-comptes.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [BackOfficeTemplateComponent, RechercheAvanceComponent, GestionDesComptesComponent ],
  imports: [
    MatTabsModule,
    MatTableModule,
    CommonModule,
    BackOfficeRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule
  ]
})
export class BackOfficeModule {
}
